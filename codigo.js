angular
	.module("myApp",[])
	.controller("PruebaController", function($scope){
		$scope.dato="";
		$scope.nuevoPensamiento="";
	})

	.controller("ListController", function(){
		var scope = this;
		scope.name= ""
		scope.lista = [];
		scope.add = function() {

			scope.lista.push(scope.name);
		}
	}) 

		.controller('EditController', function($scope) {
			$scope.userIsAdmin = true;
			$scope.message = 'Redact me';
		})

		
		//Opcion 1 para la creacion de controller
		.controller('PruebaAppController', function(){
			var scope = this;
			scope.algo = "Hola AngularJS";
		})

		//Opcion 2 para la creacion de controller
		.controller("PruebaApp2Controller", function(){
			//vm = this;
			this.algo = "Esto funciona! Gracias AngularJS";

			this.miClick = function(){
				this.algo = "Nam Miojo Rengue Kio";
			}

		})

		 .controller("acumuladorAppCtrl", function(){
				var vm = this;
				vm.total = 0;
					vm.tamTitular = "titulargran";
					vm.cuanto=0;

					vm.sumar = function(){
							 vm.total += parseInt(vm.cuanto);
					}

					vm.restar = function(){
							 vm.total -= parseInt(vm.cuanto);
					}

					vm.clases = ["uno", "dos", "tres"];
		 })

		 .controller("checkController", function(){
					var vm = this;
					vm.activo=false;
					vm.avisar = function(){
							 alert ("cambio");
							 console.log("cambio");
					}
		 })

